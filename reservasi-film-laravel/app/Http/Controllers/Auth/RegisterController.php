<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Http\Request;

class RegisterController extends Controller
{
    public function __invoke(Request $request)
    {
        $request->validate([
            'name' => 'required|string',
            'email' => 'required|email|unique:users',
            'password' => 'required|min:4',
            'provinsi' => 'required',
            'kota' => 'required',
            'kecamatan' => 'required',
            'desa' => 'required'
        ]);
    
        User::create([
            'name' => request('name'),
            'email' => request('email'),
            'password' => bcrypt(request('password')),
            'provinsi_id' => request('provinsi'),
            'kota_id' => request('kota'),
            'kecamatan_id' => request('kecamatan'),
            'desa_id' => request('desa'),
            'role' => request('role') ?? 'user'
        ]);
        
        return response()->json([
            'message' => "thanks you are registered."
        ]);
    }
}
