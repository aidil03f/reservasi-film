<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Models\{Desa,Kecamatan,Kota,Provinsi};

class RegisterFetchController extends Controller
{
    public function provinsi()
    {
        $data = Provinsi::get();
        return response()->json($data);
    }
    public function kota()
    {
        $data = Kota::get();
        return response()->json($data);
    }
    public function kecamatan()
    {
        $data = Kecamatan::get();
        return response()->json($data);
    }
    public function desa()
    {
        $data = Desa::get();
        return response()->json($data);
    }
}
