<?php

namespace App\Http\Controllers;

use App\Models\Desa;
use Illuminate\Http\Request;

class DesaController extends Controller
{
    public function index()
    {
        $data = Desa::get();
        return response()->json($data);
    }

    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required'
        ]);
        Desa::create([
            'name' => $request->name
        ]);
        return response()->json(['status' => 200]);
    }

    public function edit($id)
    {
        $data = Desa::findOrFail($id);
        return response()->json($data);
    }

    public function update(Request $request, $id)
    {
        $request->validate([
            'name' => 'required'
        ]);
        $data = Desa::findOrFail($id);
        $data->update([
            'name' => $request->name
        ]);
        return response()->json(['status' => 200]);
    }

    public function destroy($id)
    {
        Desa::find($id)->delete();
        return response()->json(['status' => 200]);
    }
}
