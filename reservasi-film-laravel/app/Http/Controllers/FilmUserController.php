<?php

namespace App\Http\Controllers;

use App\Models\Film;
use App\Http\Resources\FilmResource;

class FilmUserController extends Controller
{
   
    public function __invoke()
    {
        $data = Film::get();
         return FilmResource::collection($data);
    }
}
