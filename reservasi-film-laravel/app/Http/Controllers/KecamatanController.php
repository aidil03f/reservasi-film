<?php

namespace App\Http\Controllers;

use App\Models\Kecamatan;
use Illuminate\Http\Request;

class KecamatanController extends Controller
{
    public function index()
    {
        $data = Kecamatan::get();
        return response()->json($data);
    }

    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required'
        ]);
        Kecamatan::create([
            'name' => $request->name
        ]);
        return response()->json(['status' => 200]);
    }

    public function edit($id)
    {
        $data = Kecamatan::findOrFail($id);
        return response()->json($data);
    }

    public function update(Request $request, $id)
    {
        $request->validate([
            'name' => 'required'
        ]);
        $data = Kecamatan::findOrFail($id);
        $data->update([
            'name' => $request->name
        ]);
        return response()->json(['status' => 200]);
    }

    public function destroy($id)
    {
        Kecamatan::find($id)->delete();
        return response()->json(['status' => 200]);
    }
}
