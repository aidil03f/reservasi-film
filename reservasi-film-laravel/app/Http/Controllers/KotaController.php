<?php

namespace App\Http\Controllers;

use App\Models\Kota;
use Illuminate\Http\Request;

class KotaController extends Controller
{
    public function index()
    {
        $data = Kota::get();
        return response()->json($data);
    }

    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required'
        ]);
        Kota::create([
            'name' => $request->name
        ]);
        return response()->json(['status' => 200]);
    }

    public function edit($id)
    {
        $data = Kota::findOrFail($id);
        return response()->json($data);
    }

    public function update(Request $request, $id)
    {
        $request->validate([
            'name' => 'required'
        ]);
        $data = Kota::findOrFail($id);
        $data->update([
            'name' => $request->name
        ]);
        return response()->json(['status' => 200]);
    }

    public function destroy($id)
    {
        Kota::find($id)->delete();
        return response()->json(['status' => 200]);
    }
}
