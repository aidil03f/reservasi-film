<?php

namespace App\Http\Controllers;

use App\Models\Provinsi;
use Illuminate\Http\Request;

class ProvinsiController extends Controller
{
    public function index()
    {
        $data = Provinsi::get();
        return response()->json($data);
    }

    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required'
        ]);
        Provinsi::create([
            'name' => $request->name
        ]);
        return response('created');
    }
   
    public function edit($id)
    {
        $data = Provinsi::findOrFail($id);
        return response()->json($data);
    }

    public function update(Request $request, $id)
    {
        $request->validate([
            'name' => 'required'
        ]);
        $data = Provinsi::findOrFail($id);
        $data->update([
            'name' => $request->name
        ]);
        return response('updated');
    }

    public function destroy($id)
    {
        Provinsi::find($id)->delete();
        return response('deleted');
    }
}
