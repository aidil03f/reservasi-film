<?php

namespace App\Http\Controllers;

use App\Http\Resources\ReservasiResource;
use App\Models\Reservasi;
use Illuminate\Http\Request;

class ReservasiController extends Controller
{
  
    public function index()
    {
        $data = Reservasi::get();
        return ReservasiResource::collection($data);
    }

    public function store(Request $request)
    {
        $request->validate([
            'film' => 'required',
            'tanggal_reservasi' => 'required|date'
        ]);
        $user = auth()->user()->id;
        Reservasi::create([
            'user_id' => $user,
            'film_id' => $request->film,
            'tanggal_reservasi' => $request->tanggal_reservasi
        ]);
        return response()->json(['message' => 'created successfully']);
    }

    public function edit($id)
    {
        //
    }

    public function update(Request $request, $id)
    {
        //
    }

    public function destroy($id)
    {
        //
    }
}
