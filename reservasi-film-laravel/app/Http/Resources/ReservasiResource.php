<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class ReservasiResource extends JsonResource
{
   
    public function toArray($request)
    {
         return [
            'id' => $this->id,
            'user_id' => $this->user->name,
            'film_id' => $this->film->title,
            'tanggal_reservasi' => date('d-m-Y', strtotime($this->tanggal_reservasi))
        ];
    }
}
