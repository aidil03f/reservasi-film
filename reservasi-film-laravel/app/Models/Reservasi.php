<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Reservasi extends Model
{
    use HasFactory;
    protected $table = 'reservasi';
    protected $fillable = ['user_id','film_id','tanggal_reservasi'];

    public function user()
    {
        return $this->belongsTo(User::class,'user_id');
    }

    public function film()
    {
        return $this->belongsTo(Film::class,'film_id');
    }
}
