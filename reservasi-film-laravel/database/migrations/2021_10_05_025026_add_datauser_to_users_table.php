<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddDatauserToUsersTable extends Migration
{

    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            
            $table->foreignId('provinsi_id')->after('role')->references('id')->on('provinsi')->onDelete('cascade');
            $table->foreignId('kota_id')->after('provinsi_id')->references('id')->on('kota')->onDelete('cascade');
            $table->foreignId('kecamatan_id')->after('kota_id')->references('id')->on('kecamatan')->onDelete('cascade');
            $table->foreignId('desa_id')->after('kecamatan_id')->references('id')->on('desa')->onDelete('cascade')->nullable();
             
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->dropColumn('provinsi_id');
            $table->dropColumn('kota_id');
            $table->dropColumn('kecamatan_id');
            $table->dropColumn('desa_id');
        });
    }
}
