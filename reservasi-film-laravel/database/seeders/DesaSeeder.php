<?php

namespace Database\Seeders;

use App\Models\Desa;
use Illuminate\Database\Seeder;

class DesaSeeder extends Seeder
{
    public function run()
    {
         collect([
            'Karang joang',
            'karang batukarang',
            'ciboba'
        ])->each(function($data){
                Desa::create([
                    'name' => $data
            ]);
        });
    }
}
