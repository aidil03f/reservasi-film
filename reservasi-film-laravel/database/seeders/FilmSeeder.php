<?php

namespace Database\Seeders;

use App\Models\Film;
use Illuminate\Database\Seeder;

class FilmSeeder extends Seeder
{
    public function run()
    {
        collect([
            [
                'title' => 'Iron man',
                'start_date' => '2020-03-03',
                'end_date' => '2020-04-04',
                'description' => 'final edition'
            ],
            [
                'title' => 'Spiderman 2',
                'start_date' => '2018-02-03',
                'end_date' => '2018-07-10',
                'description' => 'best series'
            ]
        ])->each(function($data){
            Film::create($data);
        });
    }
}
