<?php

namespace Database\Seeders;

use App\Models\Kecamatan;
use Illuminate\Database\Seeder;

class KecamatanSeeder extends Seeder
{
    public function run()
    {
        collect([
            'Bandung Barat',
            'Surabaya Timur',
            'Jakarta Selatan'
        ])->each(function($data){
            Kecamatan::create([
                'name' => $data
            ]);
        });
    }
}
