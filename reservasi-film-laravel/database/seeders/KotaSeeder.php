<?php

namespace Database\Seeders;

use App\Models\Kota;
use Illuminate\Database\Seeder;

class KotaSeeder extends Seeder
{
    public function run()
    {
        collect([
            'Surabaya',
            'Bandung',
            'Yogyakarta'
        ])->each(function($data){
            Kota::create([
                'name' => $data
            ]);
        });
    }
}
