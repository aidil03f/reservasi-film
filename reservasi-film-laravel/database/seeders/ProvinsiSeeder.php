<?php

namespace Database\Seeders;

use App\Models\Provinsi;
use Illuminate\Database\Seeder;

class ProvinsiSeeder extends Seeder
{
    public function run()
    {
        collect([
            'Jawa tengah',
            'Jawa timur',
            'Jawa barat'
        ])->each(function($data){
            Provinsi::create([
                'name' => $data
            ]);
        });
    }
}
