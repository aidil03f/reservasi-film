<?php

use App\Http\Controllers\Auth\{LoginController,LogoutController,MeController,RegisterController, RegisterFetchController};
use App\Http\Controllers\{DesaController,FilmController, FilmUserController, KecamatanController,KotaController,ProvinsiController,ReservasiController};
use Illuminate\Support\Facades\Route;

Route::middleware('auth:sanctum')->group(function () {
    Route::get('me', MeController::class);
    Route::post('logout',LogoutController::class);
    Route::resources([
        'provinsi'  => ProvinsiController::class,
        'kota'      => KotaController::class,
        'kecamatan' => KecamatanController::class,
        'desa'      => DesaController::class,
        'film'      => FilmController::class,
        'reservasi' => ReservasiController::class
    ]);
});
Route::get('/kotaRegister',[RegisterFetchController::class,'kota']);
Route::get('/provinsiRegister',[RegisterFetchController::class,'provinsi']);
Route::get('/kecamatanRegister',[RegisterFetchController::class,'kecamatan']);
Route::get('/desaRegister',[RegisterFetchController::class,'desa']);
Route::get('/filmUser',FilmUserController::class);

Route::post('/register', RegisterController::class);
Route::post('/login', LoginController::class);