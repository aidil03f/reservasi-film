import React, { useEffect } from "react";
import { useRecoilState } from 'recoil'
import axios from 'axios'
import Router from "./router";
import { authenticated } from './store'

function App() {
  const [auth, setAuth] = useRecoilState(authenticated)
  const getUser = async () => {
   try {
       const response = await axios.get(`http://127.0.0.1:8000/api/me`)
       if(response.data.data.role == 'admin')
       {
         setAuth({
          check: true,
          admin: true,
          user: response.data.data
        })
       }else{
         setAuth({
           check: true,
           admin: false,
           user: response.data.data
         })
       }
   } catch (e) {}
  }
  
  useEffect(() => {
    getUser();
  },[auth.check,auth.admin])

  return <Router />
  
    
}
export default App;
