import axios from "axios";
import React from "react";
import { NavLink, useHistory } from "react-router-dom";
import { useRecoilState } from 'recoil'
import { authenticated } from "../store";

function Navigation() {
  const [auth, setAuth] = useRecoilState(authenticated)
  const redirect = useHistory()
  const Logout = async () => {
    await axios.post(`http://127.0.0.1:8000/api/logout`)
    setAuth({check: false})
    localStorage.removeItem('web-token')
    redirect.push('/login')
  }
  return (
    <div>
      <nav className="navbar navbar-expand-lg navbar-dark bg-primary border-buttom py-2">
        <div className="container-fluid">
          <NavLink className="navbar-brand" to="/">
            App
          </NavLink>
          <button
            className="navbar-toggler"
            type="button"
            data-bs-toggle="collapse"
            data-bs-target="#navbarSupportedContent"
            aria-controls="navbarSupportedContent"
            aria-expanded="false"
            aria-label="Toggle navigation"
          >
            <span className="navbar-toggler-icon"></span>
          </button>
          <div className="collapse navbar-collapse" id="navbarSupportedContent">
            <ul className="navbar-nav me-auto mb-2 mb-lg-0">
              <li className="nav-item">
                <NavLink exact className="nav-link" to="/">
                  Home
                </NavLink>
              </li>
              <li className="nav-item">
                <NavLink className="nav-link" to="/user/film">
                  Film
                </NavLink>
              </li>
            
              { auth.admin && <li className="nav-item"><NavLink className="nav-link" to="/provinsi">Provinsi</NavLink></li>}
              { auth.admin && <li className="nav-item"><NavLink className="nav-link" to="/kota">Kota</NavLink></li> }
              { auth.admin && <li className="nav-item"> <NavLink className="nav-link" to="/kecamatan">Kecamatan</NavLink></li> }
              { auth.admin && <li className="nav-item"><NavLink className="nav-link" to="/desa">Desa</NavLink></li> }    
              { auth.admin && <li className="nav-item"> <NavLink className="nav-link" to="/film">Film</NavLink></li> }
              { auth.check && <li className="nav-item"> <NavLink className="nav-link" to="/reservasi">Reservasi</NavLink></li> }
              </ul> 
                
            {
              auth.check ?
              <ul className="navbar-nav mb-2 mb-lg-0">
                <li className="nav-item">
                  <NavLink className="nav-link" to="#">
                    {auth.user.name}
                  </NavLink>
                </li>
                <li className="nav-item">
                  <NavLink className="nav-link" onClick={()=> Logout()} to="/login">
                    Logout
                  </NavLink>
                </li>
              </ul>
              :
              <ul className="navbar-nav mb-2 mb-lg-0">
                <li className="nav-item">
                  <NavLink className="nav-link" to="/login">
                    Login
                  </NavLink>
                </li>
                <li className="nav-item">
                  <NavLink className="nav-link" to="/register">
                    Register
                  </NavLink>
                </li>
              </ul>
            }
          </div>
        </div>
      </nav>
    </div>
  );
}
export default Navigation;
