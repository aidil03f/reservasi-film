import { useEffect } from "react";
import { useHistory } from "react-router-dom";
import { useRecoilValue } from 'recoil'
import { authenticated } from "../store";

const Authenticated = (props) => {
    const redirect = useHistory()
    const auth = useRecoilValue(authenticated)
    useEffect(()=> {
        if(!auth.check){
            redirect.push('/')
        }
    },[])
    return props.render
}

export default Authenticated