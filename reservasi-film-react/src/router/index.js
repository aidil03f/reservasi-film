import React from "react";
import { BrowserRouter, Route, Switch } from "react-router-dom";
import Home from "../views/Home";
import FilmUserTable from "../views/Film";
import Navigation from "../components/Navigation";
import Login from "../views/auth/Login";
import Register from "../views/auth/Register";
import ProvinsiTable from "../views/provinsi/Table";
import ProvinsiCreate from "../views/provinsi/Create";
import ProvinsiEdit from "../views/provinsi/Edit";
import KotaTable from "../views/kota/Table";
import KotaCreate from "../views/kota/Create";
import KotaEdit from "../views/kota/Edit";
import KecamatanTable from "../views/kecamatan/Table";
import KecamatanCreate from "../views/kecamatan/Create";
import KecamatanEdit from "../views/kecamatan/Edit";
import DesaTable from "../views/desa/Table";
import DesaCreate from "../views/desa/Create";
import DesaEdit from "../views/desa/Edit";
import FilmTable from "../views/film/Table";
import FilmCreate from "../views/film/Create";
import FilmEdit from "../views/film/Edit";
import Authenticated from "../middleware/Authenticated";
import ReservasiTable from "../views/reservasi/Table";
import ReservasiCreate from "../views/reservasi/Create";

function Router() {
  return (
    <div>
      <BrowserRouter>
        <Navigation />
        <div className="mt-5">
          <Switch>
            <Route exact path="/" component={Home} />
            <Route exact path="/user/film" component={FilmUserTable} />
            <Route exact path="/provinsi">
                <Authenticated render={<ProvinsiTable/>} />
            </Route>
            <Route path="/provinsi/create">
                <Authenticated render={<ProvinsiCreate/>} />
            </Route>
            <Route path="/provinsi/:id/edit">
                <Authenticated render={<ProvinsiEdit/>} />
            </Route>
            <Route exact path="/kota">
                <Authenticated render={<KotaTable/>}/>
            </Route>
            <Route path="/kota/create">
                <Authenticated render={<KotaCreate/>}/>
            </Route>
            <Route path="/kota/:id/edit">
                <Authenticated render={<KotaEdit/>}/>
            </Route>
            <Route exact path="/kecamatan">
                <Authenticated render={<KecamatanTable/>}/>
            </Route>
            <Route path="/kecamatan/create">
                <Authenticated render={<KecamatanCreate/>}/>
            </Route>
            <Route path="/kecamatan/:id/edit">
                <Authenticated render={<KecamatanEdit/>}/>
            </Route>
            <Route exact path="/desa">
                <Authenticated render={<DesaTable/>}/>
            </Route>
            <Route path="/desa/create">
                <Authenticated render={<DesaCreate/>} />
            </Route>
            <Route path="/desa/:id/edit">
                <Authenticated render={<DesaEdit/>}/>
            </Route>
            <Route exact path="/film">
                <Authenticated render={<FilmTable/>}/>
            </Route>
            <Route path="/film/create">
                <Authenticated render={<FilmCreate/>}/>
            </Route>
            <Route path="/film/:id/edit">
                <Authenticated render={<FilmEdit/>}/>
            </Route>
             <Route exact path="/reservasi">
                <Authenticated render={<ReservasiTable/>}/>
            </Route>
             <Route path="/reservasi/create">
                <Authenticated render={<ReservasiCreate/>}/>
            </Route>
            <Route path="/login" component={Login} />
            <Route path="/register">
                <Register/>
            </Route>
          </Switch>
        </div>
      </BrowserRouter>
    </div>
  );
}

export default Router;
