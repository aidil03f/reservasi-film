import { atom } from 'recoil'

const authenticated = atom ({
    key: 'authenticated',
    default: {
        check: false,
        admin: false,
        user:[]
    }
})

export { authenticated }