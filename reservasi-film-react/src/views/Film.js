import React, { useEffect, useState } from "react";
import axios from 'axios'

function FilmUserTable(){
    const [result, setFilm] = useState([]);
    
    const getFilm = async () => {
        const response = await axios.get(`http://127.0.0.1:8000/api/filmUser`);
        setFilm(response.data.data);
    };
  
    useEffect(() => {
        getFilm();
    }, [])

    return (
    <div className="row justify-content-center">
      <div className="row col-md-8">
        <div className="d-flex justify-content-between">
          <h4>Film</h4>
        </div>
        <div className="card-body">
          <table className="table table-striped">
            <thead>
              <tr>
                <th>No</th>
                <th>Nama film</th>
                <th>Mulai tayang</th>
                <th>Akhir tayang</th>
                <th>Keterangan</th>
              </tr>
            </thead>
            <tbody>
              {result.map((data, index) => {
                return (
                  <tr key={index}>
                    <td>{index + 1}</td>
                    <td>{data.title}</td>
                    <td>{data.start_date}</td>
                    <td>{data.end_date}</td>
                    <td>{data.description}</td>
                  </tr>
                );
              })}
            </tbody>
          </table>
        </div>
      </div>
    </div>
  );
}
export default FilmUserTable