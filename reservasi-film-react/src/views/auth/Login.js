import axios from "axios";
import React, { useState } from "react";
import { useRecoilState } from 'recoil'
import { NavLink, useHistory } from "react-router-dom";
import { authenticated } from '../../store'

function Login() {
  const redirect = useHistory();
  const [auth, setAuth] = useRecoilState(authenticated)
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [errors, setErrors] = useState([]);
  const record = { email, password };

  const submitHandler = async (e) => {
    e.preventDefault();
    try {
      let response = await axios.post(`http://127.0.0.1:8000/api/login`, record);
      localStorage.setItem('web-token', response.data.token)
      setAuth({ check: true, user: response.data.data })
      redirect.push("/");
    } catch (e) {
      setErrors(e.response.data.errors);
    }
  };
  return (
    <div className="container py-2">
      <div className="d-flex justify-content-center">
        <div className="card col-md-5">
          <div className="card-header">Login</div>
          <div className="card-body">
            <form onSubmit={submitHandler} autoComplete="off">
              <div className="mb-4">
                <label htmlFor="email" className="form-label">
                  Email
                </label>
                <input type="email" value={email} onChange={(e) => setEmail(e.target.value)} name="email" id="email" className="form-control" />
                {errors.email ? <div className="text-danger mt-2">{errors.email[0]}</div> : "" }
              </div>
              <div className="mb-4">
                <label htmlFor="password" className="form-label">
                  Password
                </label>
                <input type="password" value={password} onChange={(e) => setPassword(e.target.value)} name="password" id="password" className="form-control" />
                {errors.password ? <div className="text-danger mt-2">{errors.password[0]}</div> : "" }
              </div>
              <div className="d-flex justify-content-end">
                <NavLink className="btn btn-danger" to="/">
                  Back
                </NavLink>
                &nbsp;
                <button type="submit" className="btn btn-primary">
                  Save
                </button>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  );
}

export default Login;
