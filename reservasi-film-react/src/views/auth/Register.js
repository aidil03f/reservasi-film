import axios from "axios";
import React, { useEffect, useState } from "react";
import { NavLink, useHistory } from "react-router-dom";

function Register() {
  const redirect = useHistory();
  const [name, setName] = useState("");
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [kotaArr, setKota] = useState([])
  const [kota, TakeKota] = useState("")
  const [provinsiArr, setProvinsi] = useState([])
  const [provinsi, TakeProvinsi] = useState("")
  const [kecamatanArr, setKecamatan] = useState([])
  const [kecamatan, TakeKecamatan] = useState("")
  const [desaArr, setDesa] = useState([])
  const [desa, TakeDesa] = useState("")

  const [errors, setErrors] = useState([]);
  const record = { name, email, password, kota, provinsi, kecamatan, desa };
  const submitHandler = async (e) => {
    e.preventDefault();
    try {
      await axios.post(`http://127.0.0.1:8000/api/register`, record);
      redirect.push('/login')
    } catch (e) {
      setErrors(e.response.data.errors);
    }
  };
  const getKota = async () => {
    const response = await axios.get(`http://127.0.0.1:8000/api/kotaRegister`);
    setKota(response.data)
  }
  const getProvinsi = async () => {
    const response = await axios.get(`http://127.0.0.1:8000/api/provinsiRegister`);
    setProvinsi(response.data)
  }
  const getKecamatan = async () => {
    const response = await axios.get(`http://127.0.0.1:8000/api/kecamatanRegister`);
    setKecamatan(response.data)
  }
  const getDesa = async () => {
    const response = await axios.get(`http://127.0.0.1:8000/api/desaRegister`);
    setDesa(response.data)
  }


  useEffect(()=> {
    getKota();
    getProvinsi();
    getKecamatan();
    getDesa();
  },[])

  return (
    <div className="container">
      <div className="d-flex justify-content-center">
        <div className="card col-md-5">
          <div className="card-header">Register</div>
          <div className="card-body">
            <form onSubmit={submitHandler} autoComplete="off">
              <div className="mb-2">
                <label htmlFor="name" className="form-label">
                  Name
                </label>
                <input type="text" value={name} onChange={(e) => setName(e.target.value)} name="name" id="name" className="form-control" />
                {errors.name ? <div className="text-danger mt-2">{errors.name[0]}</div> : "" }
              </div>
              <div className="mb-2">
                <label htmlFor="email" className="form-label">
                  Email
                </label>
                <input type="email" value={email} onChange={(e) => setEmail(e.target.value)} name="email" id="email" className="form-control" />
                {errors.email ? <div className="text-danger mt-2">{errors.email[0]}</div> : "" }
              </div>
              <div className="mb-2">
                <label htmlFor="password" className="form-label">
                  Password
                </label>
                <input type="password" value={password} onChange={(e) => setPassword(e.target.value)} name="password" id="password" className="form-control" />
                {errors.password ? <div className="text-danger mt-2">{errors.password[0]}</div> : "" }
              </div>
              <div className="mb-2">
                  <label htmlFor="kota" className="form-label">
                    Kota
                  </label>
                  <select value={kota} onChange={(e) => TakeKota(e.target.value)} name="kota" id="kota" className="form-select">
                    <option value={null}>Choose One!</option>
                  { kotaArr.map((data) => {
                        return <option key={data.id} value={data.id}>{data.name}</option>
                    }) 
                  }
                  </select>
                   {errors.kota ? <div className="text-danger mt-2">{errors.kota[0]}</div> : "" }
                </div>
               <div className="mb-2">
                  <label htmlFor="provinsi" className="form-label">
                    Provinsi
                  </label>
                  <select value={provinsi} onChange={(e) => TakeProvinsi(e.target.value)} name="provinsi" id="provinsi" className="form-select">
                    <option value={null}>Choose One!</option>
                  { provinsiArr.map((data) => {
                        return <option key={data.id} value={data.id}>{data.name}</option>
                    }) 
                  }
                  </select>
                   {errors.provinsi ? <div className="text-danger mt-2">{errors.provinsi[0]}</div> : "" }
                </div>
                <div className="mb-2">
                  <label htmlFor="kecamatan" className="form-label">
                    Kecamatan
                  </label>
                  <select value={kecamatan} onChange={(e) => TakeKecamatan(e.target.value)} name="kecamatan" id="kecamatan" className="form-select">
                    <option value={null}>Choose One!</option>
                  { kecamatanArr.map((data) => {
                        return <option key={data.id} value={data.id}>{data.name}</option>
                    }) 
                  }
                  </select>
                   {errors.kecamatan ? <div className="text-danger mt-2">{errors.kecamatan[0]}</div> : "" }
                </div>
                <div className="mb-2">
                  <label htmlFor="desa" className="form-label">
                    Desa
                  </label>
                  <select value={desa} onChange={(e) => TakeDesa(e.target.value)} name="desa" id="desa" className="form-select">
                    <option value={null}>Choose One!</option>
                  { desaArr.map((data) => {
                        return <option key={data.id} value={data.id}>{data.name}</option>
                    }) 
                  }
                  </select>
                  {errors.desa ? <div className="text-danger mt-2">{errors.desa[0]}</div> : "" }
                </div>
              <div className="d-flex justify-content-end">
                <NavLink className="btn btn-danger" to="/">
                  Back
                </NavLink>
                &nbsp;
                <button type="submit" className="btn btn-primary">
                  Register
                </button>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  );
}

export default Register;
