import axios from "axios";
import React, { useEffect, useState } from "react";
import { NavLink, useHistory, useParams } from "react-router-dom";

function DesaEdit() {
  const { id } = useParams();
  const redirect = useHistory();
  const [errors, setErrors] = useState([]);
  const [record, setRecord] = useState({
    name: ''
  });
  const {name} = record
  const onInputChange = (e) => {
    setRecord({...record, [e.target.name]: e.target.value})
  }

  const onSubmit = async (e) => {
    e.preventDefault()
    try {
      await axios.put(`http://127.0.0.1:8000/api/desa/${id}`,record)
      redirect.push('/desa')
    } catch (e) {
      setErrors(e.response.data.errors)
    }
  }
  
  const getDesa = async () => {
       const response = await axios.get(`http://127.0.0.1:8000/api/desa/${id}/edit`)
       setRecord(response.data)
    }
    useEffect(()=> {
        getDesa();
    },[])


  return (
    <div className="container py-2">
      <div className="d-flex justify-content-center">
        <div className="card col-md-5">
          <div className="card-header">Edit desa</div>
          <div className="card-body">
            <form onSubmit={onSubmit} autoComplete="off">
              <div className="mb-4">
                <label htmlFor="name" className="form-label">
                  Name
                </label>
                <input type="text" value={name} onChange={(e) => onInputChange(e)} name="name" id="name" className="form-control" />
                {errors.name ? <div className="text-danger mt-2">{errors.name[0]}</div> : "" }
              </div>
              <div className="d-flex justify-content-end">
                <NavLink className="btn btn-danger" to="/desa">
                  Back
                </NavLink>
                &nbsp;
                <button type="submit" className="btn btn-primary">
                  Update
                </button>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  );
}

export default DesaEdit;
