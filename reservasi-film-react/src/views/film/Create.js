import axios from "axios";
import React, { useState } from "react";
import { NavLink, useHistory } from "react-router-dom";

function FilmCreate() {
  const redirect = useHistory();
  const [title, setTitle] = useState("");
  const [start_date, setStartDate] = useState("");
  const [end_date, setEndDate] = useState("");
  const [description, setDescription] = useState("");
  const [errors, setErrors] = useState([]);
  const record = { title, start_date, end_date, description };

  const submitHandler = async (e) => {
    e.preventDefault();
    try {
      await axios.post(`http://127.0.0.1:8000/api/film`, record);
      redirect.push("/film");
    } catch (e) {
      setErrors(e.response.data.errors);
    }
  };
  return (
    <div className="container py-2">
      <div className="d-flex justify-content-center">
        <div className="card col-md-5">
          <div className="card-header">Create Film</div>
          <div className="card-body">
            <form onSubmit={submitHandler} autoComplete="off">
              <div className="mb-4">
                <label htmlFor="title" className="form-label">
                  Title
                </label>
                <input type="text" value={title} onChange={(e) => setTitle(e.target.value)} name="title" id="title" className="form-control" />
                {errors.title ? <div className="text-danger mt-2">{errors.title[0]}</div> : "" }
              </div>
             <div className="mb-4">
                <label htmlFor="start_date" className="form-label">
                  Start date
                </label>
                <input type="date" value={start_date} onChange={(e) => setStartDate(e.target.value)} name="start_date" id="start_date" className="form-control" />
                {errors.start_date ? <div className="text-danger mt-2">{errors.start_date[0]}</div> : "" }
              </div>
              <div className="mb-4">
                <label htmlFor="end_date" className="form-label">
                  End date
                </label>
                <input type="date" value={end_date} onChange={(e) => setEndDate(e.target.value)} name="end_date" id="end_date" className="form-control" />
                {errors.end_date ? <div className="text-danger mt-2">{errors.end_date[0]}</div> : "" }
              </div>
              <div className="mb-4">
                <label htmlFor="description" className="form-label">
                  Description
                </label>
                <input type="text" value={description} onChange={(e) => setDescription(e.target.value)} name="description" id="description" className="form-control" />
                {errors.description ? <div className="text-danger mt-2">{errors.description[0]}</div> : "" }
              </div>
              <div className="d-flex justify-content-end">
                <NavLink className="btn btn-danger" to="/film">
                  Back
                </NavLink>
                &nbsp;
                <button type="submit" className="btn btn-primary">
                  Save
                </button>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  );
}

export default FilmCreate;
