import axios from "axios";
import React, { useEffect, useState } from "react";
import { NavLink, useHistory, useParams } from "react-router-dom";

function FilmEdit() {
  const { id } = useParams();
  const redirect = useHistory();
  const [errors, setErrors] = useState([]);
  const [record, setRecord] = useState({
    title: '',
    start_date: '',
    end_date: '',
    description: ''
  });
  const { title, start_date, end_date, description } = record
  const onInputChange = (e) => {
    setRecord({...record, [e.target.name]: e.target.value})
  }

  const onSubmit = async (e) => {
    e.preventDefault()
    try {
      await axios.put(`http://127.0.0.1:8000/api/film/${id}`,record)
      redirect.push('/film')
    } catch (e) {
      setErrors(e.response.data.errors)
    }
  }
  
  const getFilm = async () => {
       const response = await axios.get(`http://127.0.0.1:8000/api/film/${id}/edit`)
       setRecord(response.data)
    }
    useEffect(()=> {
        getFilm();
    },[])


  return (
    <div className="container py-2">
      <div className="d-flex justify-content-center">
        <div className="card col-md-5">
          <div className="card-header">Edit Film</div>
          <div className="card-body">
            <form onSubmit={onSubmit} autoComplete="off">
              <div className="mb-4">
                <label htmlFor="title" className="form-label">
                  Title
                </label>
                <input type="text" value={title} onChange={(e) => onInputChange(e)} name="title" id="title" className="form-control" />
                {errors.title ? <div className="text-danger mt-2">{errors.title[0]}</div> : "" }
              </div>
              <div className="mb-4">
                <label htmlFor="start_date" className="form-label">
                  Start date
                </label>
                <input type="date" value={start_date} onChange={(e) => onInputChange(e)} name="start_date" id="start_date" className="form-control" />
                {errors.start_date ? <div className="text-danger mt-2">{errors.start_date[0]}</div> : "" }
              </div>
              <div className="mb-4">
                <label htmlFor="end_date" className="form-label">
                  End date
                </label>
                <input type="date" value={end_date} onChange={(e) => onInputChange(e)} name="end_date" id="end_date" className="form-control" />
                {errors.end_date ? <div className="text-danger mt-2">{errors.end_date[0]}</div> : "" }
              </div>
              <div className="mb-4">
                <label htmlFor="description" className="form-label">
                  Description
                </label>
                <input type="text" value={description} onChange={(e) => onInputChange(e)} name="description" id="description" className="form-control" />
                {errors.description ? <div className="text-danger mt-2">{errors.description[0]}</div> : "" }
              </div>
              <div className="d-flex justify-content-end">
                <NavLink className="btn btn-danger" to="/film">
                  Back
                </NavLink>
                &nbsp;
                <button type="submit" className="btn btn-primary">
                  Update
                </button>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  );
}

export default FilmEdit;
