import React, { useEffect, useState } from "react";
import axios from 'axios'
import { NavLink } from "react-router-dom";

function FilmTable(){
    const [result, setFilm] = useState([]);
    
    const getFilm = async () => {
        const response = await axios.get(`http://127.0.0.1:8000/api/film`);
        setFilm(response.data.data);
    };
  
    useEffect(() => {
        getFilm();
    }, [])

    const deleteFilm = async (id) => {
        if(window.confirm('Are you sure ?'))
        {
            const response = await axios.delete(`http://127.0.0.1:8000/api/film/${id}`)
            getFilm(response.data.data)
        }
    }
    return (
    <div className="row justify-content-center">
      <div className="row col-md-8">
        <div className="d-flex justify-content-between">
          <h4>Film</h4>
          <NavLink className="btn btn-primary" to="/film/create">
            Create
          </NavLink>
        </div>
        <div className="card-body">
          <table className="table table-striped">
            <thead>
              <tr>
                <th>No</th>
                <th>Nama film</th>
                <th>Mulai tayang</th>
                <th>Akhir tayang</th>
                <th>Keterangan</th>
                <th>Action</th>
              </tr>
            </thead>
            <tbody>
              {result.map((data, index) => {
                return (
                  <tr key={index}>
                    <td>{index + 1}</td>
                    <td>{data.title}</td>
                    <td>{data.start_date}</td>
                    <td>{data.end_date}</td>
                    <td>{data.description}</td>
                    <td>
                      <NavLink to={`/film/${data.id}/edit`} className="btn btn-warning btn-sm">
                        Edit
                      </NavLink>
                      &nbsp;&nbsp;
                      <button className="btn btn-danger btn-sm" onClick={() => deleteFilm(data.id)} >
                        Delete
                      </button>
                    </td>
                  </tr>
                );
              })}
            </tbody>
          </table>
        </div>
      </div>
    </div>
  );
}
export default FilmTable