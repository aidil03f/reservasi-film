import axios from "axios";
import React, { useState } from "react";
import { NavLink, useHistory } from "react-router-dom";

function KecamatanCreate() {
  const redirect = useHistory();
  const [name, setName] = useState("");
  const [errors, setErrors] = useState([]);
  const record = { name };

  const submitHandler = async (e) => {
    e.preventDefault();
    try {
      await axios.post(`http://127.0.0.1:8000/api/kecamatan`, record);
      redirect.push("/kecamatan");
    } catch (e) {
      setErrors(e.response.data.errors);
    }
  };
  return (
    <div className="container py-2">
      <div className="d-flex justify-content-center">
        <div className="card col-md-5">
          <div className="card-header">Create Kecamatan</div>
          <div className="card-body">
            <form onSubmit={submitHandler} autoComplete="off">
              <div className="mb-4">
                <label htmlFor="name" className="form-label">
                  Name
                </label>
                <input type="text" value={name} onChange={(e) => setName(e.target.value)} name="name" id="name" className="form-control" />
                {errors.name ? <div className="text-danger mt-2">{errors.name[0]}</div> : "" }
              </div>
              <div className="d-flex justify-content-end">
                <NavLink className="btn btn-danger" to="/kecamatan">
                  Back
                </NavLink>
                &nbsp;
                <button type="submit" className="btn btn-primary">
                  Save
                </button>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  );
}

export default KecamatanCreate;
