import React, { useEffect, useState } from "react";
import axios from "axios";
import { NavLink } from "react-router-dom";

function KecamatanTable() {
  const [result, setKecamatan] = useState([]);
  
  const getKecamatan = async () => {
    const response = await axios.get(`http://127.0.0.1:8000/api/kecamatan`);
    setKecamatan(response.data);
  };
  
  useEffect(() => {
    getKecamatan();
  }, [])
  
  const deleteKecamatan = async (id) => {
    if(window.confirm('Are you sure ?'))
    {
        const response = await axios.delete(`http://127.0.0.1:8000/api/kecamatan/${id}`)
        getKecamatan(response.data)
    }
  }

  return (
    <div className="row justify-content-center">
      <div className="row col-md-8">
        <div className="d-flex justify-content-between">
          <h4>Kecamatan</h4>
          <NavLink className="btn btn-primary" to="/kecamatan/create">
            Create
          </NavLink>
        </div>
        <div className="card-body">
          <table className="table table-striped">
            <thead>
              <tr>
                <th>No</th>
                <th>Nama Kecamatan</th>
                <th>Action</th>
              </tr>
            </thead>
            <tbody>
              {result.map((data, index) => {
                return (
                  <tr key={index}>
                    <td>{index + 1}</td>
                    <td>{data.name}</td>
                    <td>
                      <NavLink to={`/kecamatan/${data.id}/edit`} className="btn btn-warning btn-sm">
                        Edit
                      </NavLink>
                      &nbsp;&nbsp;
                      <button className="btn btn-danger btn-sm" onClick={() => deleteKecamatan(data.id)} >
                        Delete
                      </button>
                    </td>
                  </tr>
                );
              })}
            </tbody>
          </table>
        </div>
      </div>
    </div>
  );
}
export default KecamatanTable;
