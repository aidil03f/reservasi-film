import React, { useEffect, useState } from "react";
import axios from "axios";
import { NavLink } from "react-router-dom";

function ProvinsiTable() {
  const [result, setProvinsi] = useState([]);
  
  const getProvinsi = async () => {
    const response = await axios.get(`http://127.0.0.1:8000/api/provinsi`);
    setProvinsi(response.data);
  };
  
  useEffect(() => {
    getProvinsi();
  }, [])
  
  const deleteProvinsi = async (id) => {
    if(window.confirm('Are you sure ?'))
    {
        const response = await axios.delete(`http://127.0.0.1:8000/api/provinsi/${id}`)
        getProvinsi(response.data)
    }
  }

  return (
    <div className="row justify-content-center">
      <div className="row col-md-8">
        <div className="d-flex justify-content-between">
          <h4>Provinsi</h4>
          <NavLink className="btn btn-primary" to="/provinsi/create">
            Create
          </NavLink>
        </div>
        <div className="card-body">
          <table className="table table-striped">
            <thead>
              <tr>
                <th>No</th>
                <th>Nama Provinsi</th>
                <th>Action</th>
              </tr>
            </thead>
            <tbody>
              {result.map((data, index) => {
                return (
                  <tr key={index}>
                    <td>{index + 1}</td>
                    <td>{data.name}</td>
                    <td>
                      <NavLink to={`/provinsi/${data.id}/edit`} className="btn btn-warning btn-sm">
                        Edit
                      </NavLink>
                      &nbsp;&nbsp;
                      <button className="btn btn-danger btn-sm" onClick={() => deleteProvinsi(data.id)} >
                        Delete
                      </button>
                    </td>
                  </tr>
                );
              })}
            </tbody>
          </table>
        </div>
      </div>
    </div>
  );
}
export default ProvinsiTable;
