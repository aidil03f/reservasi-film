import axios from "axios";
import React, { useEffect, useState } from "react";
import { NavLink, useHistory  } from "react-router-dom";

function ReservasiCreate() {
  const redirect = useHistory()
  const [films, setFilm] = useState([]);
  const [errors, setErrors] = useState([]);
  const [film, setFilmId] = useState('')
  const [tanggal_reservasi, setTglreservasi] = useState('')
  const record = { film, tanggal_reservasi }

  const submitHandler = async (e) => {
     e.preventDefault();
    try {
       await axios.post(`http://127.0.0.1:8000/api/reservasi`, record)
       redirect.push('/reservasi')
    } catch (e) {
        setErrors(e.response.data.errors);
    }
  }
  const getFilm = async () => {
    const response = await axios.get(`http://127.0.0.1:8000/api/film`)
    setFilm(response.data.data)
  }
  useEffect(()=> {
    getFilm()
  },[])


  
  return (
    <div className="container py-2">
      <div className="d-flex justify-content-center">
        <div className="card col-md-5">
          <div className="card-header">Create Reservasi</div>
          <div className="card-body">
            <form onSubmit={submitHandler} autoComplete="off">
                <div className="mb-4">
                  <label htmlFor="film" className="form-label">
                    Film
                  </label>
                  <select value={film} onChange={(e) => setFilmId(e.target.value)} name="film" id="film" className="form-select">
                    <option disabled selected value="">Choose a film</option>
                  { films.map((data) => {
                        return <option key={data.id} value={data.id}>{data.title}</option>
                    }) 
                  }
                  </select>
                   {errors.film ? <div className="text-danger mt-2">{errors.film[0]}</div> : "" }
                </div>
                <div className="mb-4">
                  <label htmlFor="tanggal_reservasi" className="form-label">
                    Tanggal reservasi
                  </label>
                  <input type="date" value={tanggal_reservasi} onChange={(e) => setTglreservasi(e.target.value)} name="tanggal_reservasi" id="tanggal_reservasi" className="form-control" />
                  {errors.tanggal_reservasi ? <div className="text-danger mt-2">{errors.tanggal_reservasi[0]}</div> : "" }
                </div>
              <div className="d-flex justify-content-end">
                <NavLink className="btn btn-danger" to="/reservasi">
                  Back
                </NavLink>
                &nbsp;
                <button type="submit" className="btn btn-primary">
                  Save
                </button>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  );
}

export default ReservasiCreate;
