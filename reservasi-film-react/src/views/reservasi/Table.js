import React, { useEffect, useState } from "react";
import axios from "axios";
import { NavLink } from "react-router-dom";

function ReservasiTable() {
  const [result, setReservasi] = useState([]);
  
  const getReservasi = async () => {
    const response = await axios.get(`http://127.0.0.1:8000/api/reservasi`);
    setReservasi(response.data.data);
  };
  
  useEffect(() => {
    getReservasi();
  }, [])

  return (
    <div className="row justify-content-center">
      <div className="row col-md-8">
        <div className="d-flex justify-content-between">
          <h4>Reservasi</h4>
          <NavLink className="btn btn-primary" to="/reservasi/create">
            Create
          </NavLink>
        </div>
        <div className="card-body">
          <table className="table table-striped">
            <thead>
              <tr>
                <th>#</th>
                <th>nama user</th>
                <th>nama film</th>
                <th>tanggal reservasi</th>
              </tr>
            </thead>
            <tbody>
              {result.map((data, index) => {
                return (
                  <tr key={index}>
                    <td>{index + 1}</td>
                    <td>{data.user_id}</td>
                    <td>{data.film_id}</td>
                    <td>{data.tanggal_reservasi}</td>
                  </tr>
                );
              })}
            </tbody>
          </table>
        </div>
      </div>
    </div>
  );
}
export default ReservasiTable;
